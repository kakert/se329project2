<?php
namespace Food\Controller;
use Food\Form\UserForm;
use Food\Form\LoginForm;
use Food\Form\CategoriesForm;
use Food\Model\User;
use Food\Model\Type;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Config\SessionConfig;
use Zend\Session\Container;
use Zend\Session\SessionManager;


class UserController extends AbstractActionController
{
	protected $userTable;
	protected $typeTable;
	protected $user;
	public function onDispatch( \Zend\Mvc\MvcEvent $e )
	{
		$session = new Container('user');
		if ($session && $session->uid) 
		{
			$this->user = $session->uid;
		}
		return parent::onDispatch( $e );
	}

	public function initSession($config)
	{
	    $sessionConfig = new SessionConfig();
	    $sessionConfig->setOptions($config);
	    $sessionManager = new SessionManager($sessionConfig);
	    $sessionManager->start();
	    Container::setDefaultManager($sessionManager);
	}	
	public function getUserTable()
	{
		if(!$this->userTable)
		{
			$sm = $this->getServiceLocator();
			$this->userTable = $sm->get('Food\Model\UserTable');
		}
		return $this->userTable;
	}
	public function getTypeTable()
	{
		if(!$this->typeTable)
		{
			$sm = $this->getServiceLocator();
			$this->typeTable = $sm->get('Food\Model\TypeTable');
		}
		return $this->typeTable;
	}

	public function indexAction()
	{
		$form = new LoginForm();
		$form->get('submit')->setValue('Login');

		$request = $this->getRequest();
		if ($request->isPost()) {
			$user = new User();
			$form->setInputFilter($user->getInputFilter());
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$data = $form->getData();
				$userExists=$this->getUserTable()->validateUser($data['username'],$data['password']);
				// Redirect to a view of this reservation
				if($userExists){
					$this->initSession(array(
					    'remember_me_seconds' => 600,
					    'use_cookies' => true,
					    'cookie_httponly' => true,
					));
					$sessionUser = new Container('user');
					$sessionUser->uid = $userExists;
					return $this->redirect()->toRoute('food', array('action' => 'index'));
				}
				else
					$this->flashMessenger()->addMessage('User or Password Not found');
			}
		}

		return array(	'form' => $form,
				'messages' => $this->flashMessenger()->getCurrentMessages());
   	}

	public function addAction()
	{
		$form = new LoginForm();
		$form->get('submit')->setValue('Create Account');

		$request = $this->getRequest();
		if ($request->isPost()) {
			$user = new User();
			$form->setInputFilter($user->getInputFilter());
			$form->setData($request->getPost());

			if ($form->isValid()) {
				$data = $form->getData();
				$id = $this->getUserTable()->getUserByName($data['username']);
				if(!$id)
				{
					$user->exchangeArray($data);
					$id = $this->getUserTable()->saveUser($user);
					$userExists=$this->getUserTable()->validateUser($data['username'],$data['password']);
					if($userExists){
						$this->initSession(array(
						    'remember_me_seconds' => 600,
						    'use_cookies' => true,
						    'cookie_httponly' => true,
						));
						$sessionUser = new Container('user');
						$sessionUser->uid = $userExists;
					}
					return $this->redirect()->toRoute('food', array('action' => 'index'));
				}
				else
				{
					$this->flashMessenger()->addMessage('Username already exists');
				}
			}
		}
		return array(	'form' => $form,
				'messages' => $this->flashMessenger()->getCurrentMessages());
	}
  	public function editAction()
	{

		$id = $this->user->id;
		if (!$id) {
			return $this->redirect()->toRoute('user', array(
				'action' => 'add'
			));
		}
		$user = $this->getUserTable()->getUser($id);
		$types = $this->getTypeTable()->fetchAll();
		$options = array();
		foreach($types as $type)
		{
			$options[$type->id]=$type->name;
		}
		$userTypes = $this->getTypeTable()->getuserTypes($user->types);
		$form  = new UserForm();
		$form->bind($user);
		$form->get('submit')->setAttribute('value', 'Edit');
		$form->get('types')->setValue(implode(',',$userTypes));
		

		$categoryForm = new CategoriesForm();
		$categoryForm->get('submit')->setAttribute('value', 'Add');
		$categoryForm->setAttribute('action', $this->url()->fromRoute('user', ['action' => 'create-type']));

		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter($user->getInputFilter());
			$form->setData($request->getPost());

			if ($form->isValid()) {
				$data = $form->getData();
				// $userExists = $this->getUserTable()->getUserByName($data->username);
				$userExists=$this->getUserTable()->validateUser($data->username, $data->oldPassword);
				if(!$userExists)
				{
					$data->types = $request->getPost()->types;
					$this->getUserTable()->saveUser($data);
	
					// Redirect to list of users
					return $this->redirect()->toRoute('food');
				}
				else
				{
					$this->flashMessenger()->addMessage('Username already exists');
				}
			}
		}

		return array(
			'id' => $id,
			'form' => $form,
			'categoryForm' => $categoryForm,
			'types' => $options,
			'messages' => $this->flashMessenger()->getCurrentMessages());
	}

	/**
	 * Expects a POST from the category form
	 * @return [type] [description]
	 */
	public function createTypeAction()
	{
		$request = $this->getRequest();
		$categoryForm = new CategoriesForm();
		$categoryForm->setData($request->getPost());

		if($categoryForm->isValid()) {
			$data = $categoryForm->getData();
			$this->getTypeTable()->saveType($data);
		}
		return $this->redirect()->toRoute('user', ['action' => 'edit', 'id' => $this->user->id]);
	}

	public function deleteAction()
	{
	}
	public function viewAction()
	{
	}
}
