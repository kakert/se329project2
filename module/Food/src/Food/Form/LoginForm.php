<?php
namespace Food\Form;

use Zend\Form\Form;

class LoginForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('user');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name'=>'id',
			'type'=>'Hidden',
		));
		$this->add(array(
			'name'=> 'username',
			'type' => 'Text',
			'options'=>array(
				'label'=> 'Username:',
			),
			'attributes' => array( 
				'class' => 'form-control',
				'placeholder' => 'Username'
			)
		));
		$this->add(array(
			'name'=> 'password',
			'type' => 'Text',
			'options'=>array(
				'label'=> 'Password: ',
			),
			'attributes' => array( 
				'class' => 'form-control',
				'placeholder' => 'Password'
			)
		));
		$this->add(array(
			'name'=>'submit',
			'type'=>'submit',
			'attributes'=>array(
				'value'=>'Go',
				'class' => 'btn btn-primary',
				'id'=>'submitbutton',
			),
		));

	}
}
