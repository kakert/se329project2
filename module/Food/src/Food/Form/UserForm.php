<?php
namespace Food\Form;

use Zend\Form\Form;

class UserForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('user');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name'=>'id',
			'type'=>'Hidden',
		));
		$this->add(array(
			'name'=> 'username',
			'type' => 'Text',
			'options'=>array(
				'label'=> 'Username ',
			),
			'attributes' => array(
				'class' => 'form-control ',
				)
		));
		$this->add(array(
			'name'=>'types',
			'type'=>'Hidden',
			'attributes' => array(
				'class' => 'hidden-types'
			)
		));
		$this->add(array(
			'name'=> 'oldPassword',
			'type' => 'Password',
			'options'=>array(
				'label'=> 'Current Password ',
			),
			'attributes' => array(
				'class' => 'form-control floating-label',
				'placeholder' => 'Password'
				)
		));
		$this->add(array(
			'name'=> 'password',
			'type' => 'Password',
			'options'=>array(
				'label'=> 'New Password ',
			),
			'attributes' => array(
				'class' => 'form-control floating-label',
				'placeholder' => 'Password'
				)
		));
		$this->add(array(
			'name'=>'submit',
			'type'=>'submit',
			'attributes'=>array(
				'value'=>'Go',
				'id'=>'submitbutton',
				'class' => 'btn btn-primary'
			),
		));

	}
}
