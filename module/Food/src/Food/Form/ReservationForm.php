<?php
namespace Food\Form;

use Zend\Form\Form;

class ReservationForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('reservation');

		$this->setAttribute('method', 'post');

		$this->add(array(
			'name'=>'id',
			'type'=>'Hidden',
		));

		$this->add(array(
			'name'=>'creator',
			'type'=>'Hidden',
		));

		$this->add(array(
			'name'=>'location',
			'type'=>'Hidden',
		));

		$this->add(array(
			'name'=> 'title',
			'type' => 'Text',
			'attributes' => array(
				'class' => 'form-control floating-label',
				'placeholder' => 'Title'
			)
		));

		$this->add(array(
			'name'=>'submit',
			'type'=>'submit',
			'attributes'=>array(
				'value'=>'Go',
				'id'=>'submitbutton',
			),
		));
	}
}
