<?php
namespace Food\Form;

use Zend\Form\Form;

class CategoriesForm extends Form
{
	public function __construct($name = null)
	{
		parent::__construct('user');
		$this->setAttribute('method', 'post');
		$this->add(array(
             'name' => 'id',
             'type' => 'Hidden',
        ));
		$this->add(array(
			'name'=> 'name',
			'type' => 'Text',
			'options'=>array(
				'label'=> 'Custom Category:',
			),
			'attributes' => array(
				'class' => 'form-control',
				'placeholder' => 'Category name'
			)
		));
		$this->add(array(
			'name'=>'submit',
			'type'=>'submit',
			'attributes'=>array(
				'value'=>'Go',
				'class' => 'btn btn-success',
				'id'=>'submitbutton',
			),
		));

	}
}
