-- Creates the database user with a database
CREATE USER 'restaurant'@'localhost' IDENTIFIED BY 'se329';
CREATE DATABASE restaurant;
GRANT ALL PRIVILEGES ON restaurant.* TO 'restaurant'@'localhost';
