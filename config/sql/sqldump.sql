-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: restaurant
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `food`
--

DROP TABLE IF EXISTS `food`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food`
--

LOCK TABLES `food` WRITE;
/*!40000 ALTER TABLE `food` DISABLE KEYS */;
INSERT INTO `food` VALUES (1,'Gateway Hotel and Conference Center',42.001021,-93.642798,7),(2,'Hickory Park Restaurant Co',42.010025,-93.609495,3),(3,'Olde Main Brewing Company',42.024852,-93.614752,3),(4,'The Cafe',42.048679,-93.643671,6),(5,'SUBWAYÃ‚Â® Restaurants',42.022199,-93.650441,10),(6,'SUBWAYÃ‚Â® Restaurants',42.054479,-93.621006,10),(7,'Perfect Games',42.010665,-93.681144,7),(8,'El Azteca',42.008749,-93.580513,2),(9,'Panera Bread',42.019829,-93.611071,3),(10,'Subway',42.011242,-93.61292,10),(11,'Jimmy John\'s',42.049793,-93.622437,10),(12,'The Great Plains Sauce & Dough Company',42.025231,-93.611804,9),(13,'SUBWAYÃ‚Â® Restaurants (College / University)',42.023521,-93.645738,10),(14,'Pizza Ranch',42.035794,-93.582156,9),(15,'SUBWAYÃ‚Â® Restaurants',42.008824,-93.585971,10),(16,'Black Market Pizza',42.048712,-93.643151,9),(17,'Jimmy John\'s Gourmet Sandwiches',42.021589,-93.650449,10),(18,'Aunt Maude\'s',42.025177,-93.619065,7),(19,'McDonald\'s',42.023306,-93.668578,8),(20,'Pammel Grocery',42.023406,-93.663549,7),(21,'West Towne Pub',42.011129,-93.679182,3),(22,'Vesuvius Wood Fired Pizza',42.006655,-93.611216,9),(23,'Jeff\'s Pizza Shop',42.022628,-93.648683,9),(24,'Dublin Bay',42.008136,-93.614658,7),(25,'Perkins Restaurant & Bakery',42.019115,-93.610977,3),(26,'Ge-Angelo\'s Italian Restaurant',42.054907,-93.623004,7),(27,'The Fighting Burrito',42.02205,-93.65046,2),(28,'Valentino\'s',42.05411,-93.623007,9),(29,'Pizza Pit',42.021123,-93.650504,9),(30,'Pizza Hut',42.017679,-93.609814,9),(31,'Mongolian Buffet',42.006671,-93.611254,1),(32,'Grove Cafe',42.025058,-93.611443,6),(33,'Culver\'s',42.023825,-93.618733,8),(34,'Lucullan\'s',42.024872,-93.615581,7),(35,'Fuji Japanese Steak House',42.006889,-93.611681,7),(36,'Mandarin Chinese Restaurant',42.023046,-93.616085,1),(37,'Wallaby\'s Bar & Grill',42.022537,-93.668608,3),(38,'B-Bop\'s',42.012602,-93.609512,8),(39,'King Buffet',42.011242,-93.61292,1),(40,'DAIRY QUEEN STORE',42.022626,-93.614315,8),(41,'Dairy Queen',42.05492,-93.621302,8),(42,'Ames Cupcake Emporium',42.02498,-93.613834,7),(43,'McDonald\'s',42.021906,-93.61077,8),(44,'Old Chicago',42.007535,-93.610829,9),(45,'House Of Chen',42.022904,-93.604156,1),(46,'KFC Ames',42.02321,-93.617187,8),(47,'New China Restaurant',42.022474,-93.667945,1),(48,'Pizza Hut',42.021563,-93.618191,9),(49,'Panda Express',42.023857,-93.645799,8),(50,'Thai Kitchen',42.021344,-93.649186,4),(51,'The IowaStater Restaurant',42.001077,-93.643007,3),(52,'The Spice Thai Cuisine',42.024978,-93.615629,4),(53,'Taco John\'s',42.022671,-93.617247,2),(54,'Cafe Northwest',42.023439,-93.609317,6),(55,'Le\'s Vietnamese Restaurant',42.023406,-93.663549,1),(56,'India Palace',42.021976,-93.651283,5),(57,'Whiskey River',42.025051,-93.611786,3),(58,'Jimmy John\'s Gourmet Sandwiches',42.010799,-93.609359,10),(59,'Cafe Beaudelaire',42.022644,-93.650414,6),(60,'Kingpin Pizza',42.010862,-93.681324,9),(61,'El Azteca',42.050018,-93.645579,2),(62,'Papa Murphy\'s Take \'N\' Bake Pizza',42.023,-93.6109,9),(63,'Chinese Home Style Cooking',42.022423,-93.654705,1),(64,'SUBWAYÃ‚Â® Restaurants',42.021587,-93.668812,10),(65,'May House Cuisine',42.023104,-93.665787,1),(66,'Buffalo Wild Wings',42.019014,-93.609497,3),(67,'Downtown Deli',42.024991,-93.614874,10),(68,'West Street Deli',42.025247,-93.656255,10),(69,'Texas Roadhouse',42.016239,-93.611028,3),(70,'Little Taipei',42.02124,-93.650759,4),(71,'Pancheros Mexican Grill',42.010779,-93.609438,2),(72,'Red Lobster',42.013064,-93.6109,7),(73,'Mickey\'s Irish Pub',42.022358,-93.650259,7),(74,'Okoboji Grill Ã¢â‚¬â€  Ames',42.022335,-93.609941,3),(75,'Clyde\'s Sports Club',42.024988,-93.651519,3),(76,'Union Drive Marketplace',42.025043,-93.651483,3),(77,'Jimmy John\'s Gourmet Sandwiches',42.011055,-93.679643,10),(78,'Burger King',42.022941,-93.612542,8),(79,'La Fuente Restaurant',42.020952,-93.610652,2),(80,'Flame and Skewer',42.049803,-93.622296,3),(81,'Battle\'s Bar-B-Q',42.020697,-93.649658,3),(83,'Mr Burrito',42.022645,-93.650523,2),(84,'Taco Time',42.023147,-93.61749,2),(85,'Arby\'s',42.018595,-93.610921,8),(86,'Wendy\'s',42.01639,-93.609661,8),(87,'Arby\'s',42.033796,-93.577091,8),(88,'Bookends Cafe',42.028089,-93.648722,6),(89,'Business Cafe',42.025116,-93.644435,6),(90,'Taco Bell',42.01812,-93.610924,2),(91,'Applebee\'s',42.011872,-93.610993,3),(92,'Fazoli\'s',42.046295,-93.62062,8),(93,'Noodles & Company',42.018427,-93.609386,7),(94,'Arcadia Cafe',42.022368,-93.654892,6),(95,'Scallion Korean Restaurant',42.022116,-93.651486,1),(96,'Indian Delights',42.021861,-93.671503,5),(97,'Design Cafe',42.028439,-93.653168,6),(98,'Odd Fellows Burger Kitchen',42.05475,-93.623112,3),(99,'Conversations Dining',42.024909,-93.639725,3),(100,'Bufords Steak House & BBQ',42.03485,-93.578452,3),(101,'888 Chinese Restaurant',42.054859,-93.621976,1),(102,'Gentle Doctor Cafe',42.006344,-93.632032,6),(103,'Hawthorn - Market Ã‚Â· Grill Ã‚Â· Cafe',42.033809,-93.642885,6),(104,'Memorial Union Food Court',42.023386,-93.645819,8),(105,'Seasons Marketplace',42.024958,-93.65149,3),(106,'Courtyard Cafe',42.029911,-93.645608,6),(107,'Brick City Grill',42.049137,-93.644766,3),(108,'Shogun',42.022685,-93.668023,7),(109,'Hub Grill and Cafe',42.027164,-93.648444,6),(110,'Chipotle Mexican Grill',42.017666,-93.610979,2),(111,'Joy\'s Mongolian Grill',42.022274,-93.651587,1),(112,'Joe\'s Pizza & Pasta',42.011128,-93.679316,9),(113,'Dairy Queen',42.009125,-93.585954,8),(114,'McDonald\'s',42.016253,-93.606969,8),(115,'Froots',42.024842,-93.653581,8),(116,'+39 Restaurant, Market, & Cantina',42.048672,-93.644686,7),(117,'Hungry4Huh LLC - Ames',42.021541,-93.651105,7),(118,'Godfather\'s Pizza',42.024081,-93.650191,9),(119,'Memorial Union Market and Cafe',42.023386,-93.645819,6),(120,'Munchiez Snack Stop',42.022568,-93.648833,8),(121,'Global Cafe',42.026182,-93.644537,6),(122,'Godfather\'s Pizza',42.023327,-93.645455,9),(123,'Taco Time',42.023169,-93.617466,2),(124,'Burger King',42.034914,-93.57372,8),(125,'Es Tas Bar & Grill',42.020568,-93.648463,2),(126,'Mangostinos Bar and Grill',42.022591,-93.603933,3),(127,'Smokin Big Dawgs BBQ & Caterin',42.026177,-93.736513,3),(128,'Village Inn',42.022625,-93.618053,3),(129,'Subway',42.022482,-93.613915,10),(130,'201 Market Street Ames, Iowa 50010',42.023463,-93.612992,7),(131,'Indian Delight Express',42.021951,-93.649934,5),(132,'Cazador Mexican Restaurant',42.023318,-93.667377,2),(133,'PepperJax Grill',42.018427,-93.609391,3);
/*!40000 ALTER TABLE `food` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `creator` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (1,'test','13',4),(2,'test35','39',1),(3,'American','66',3),(4,'Everyone','55',0),(6,'Pizza change','29',0),(7,'Pizza no','30',0),(9,'anotherTest','96',19);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'Chinese'),(2,'Mexican'),(3,'American'),(4,'Thai'),(5,'Indian'),(6,'Cafe'),(7,'Other'),(8,'Fast Food'),(9,'Pizza'),(10,'Sandwiches');
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `types` varchar(255) DEFAULT NULL,
  `shared` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'james','password','1,6','2,3,4'),(2,'james2','password',NULL,NULL),(3,'Mexico','password','3',NULL),(4,'user','password','10',NULL),(5,'test23','password',NULL,NULL),(6,'test1123','password',NULL,NULL),(7,'jastuder','testing','1','4'),(8,'1234','password','1','5'),(9,'nlockard','test1','1,2,3,4,5,6,7,8,9,10',NULL),(10,'nlockard3','test','1',NULL),(11,'nlockard4','test','3','6'),(12,'nlockard1','test1','9','8'),(13,'user','password',NULL,NULL),(14,'user','password',NULL,NULL),(15,'user','password',NULL,NULL),(16,'user','user',NULL,NULL),(17,'userasdfasd','user','10',NULL),(18,'demo','demo',NULL,NULL),(19,'austin2','dorenkamp2','3,5,7,8,9',NULL),(20,'greg','hart',NULL,'8,9');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-20 17:01:54
